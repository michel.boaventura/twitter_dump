require 'tweetstream'
require 'twitter'
require 'yaml'
require 'mongo'
require 'json'
require 'pry'

include Mongo

my_config = YAML.load_file('config.yaml')

db     = MongoClient.new.db(my_config['database'])
tweets = db.collection(my_config['tweets'])
users  = db.collection(my_config['users'])

TweetStream.configure do |config|
  config.consumer_key       = my_config['consumer_key']
  config.consumer_secret    = my_config['consumer_secret']
  config.oauth_token        = my_config['oauth_token']
  config.oauth_token_secret = my_config['oauth_token_secret']
  config.auth_method        = :oauth
end

Twitter.configure do |config|
  config.consumer_key       = my_config['consumer_key']
  config.consumer_secret    = my_config['consumer_secret']
  config.oauth_token        = my_config['oauth_token']
  config.oauth_token_secret = my_config['oauth_token_secret']
end

def get_followers(id)
  followers = []
  list = Twitter.followers(id)

  while (next_cursor = list.next_cursor) != 0
    puts "Cursor: #{next_cursor}"
    followers += list.attrs[:users]
    list = Twitter.followers(id, cursor: next_cursor)
  end
  return followers
end

TweetStream::Client.new.track(my_config['tags']) do |status|
  #Getting tweet and user
  tweet = status.attrs
  user  = tweet.delete(:user)

  #Minor tweaks
  user[:_id] = user.delete(:id)
  tweet[:_id] = tweet.delete(:id)
  tweet[:user_id] = user[:_id]

  ##Get followers
  #followers = get_followers(user[:_id])
  #followers_ids = followers.map{|u| u[:id]}

  ##Put followers ids on tweet
  #tweet[:followers_ids] = followers_ids

  ##'Upsert' each follower as a regular user
  #followers.each do |follower|
  #  follower[:_id] = follower.delete(:id)
  #  users.update({_id: follower[:_id]}, follower, upsert: true)
  #end

  #Insert tweet and 'upsert' user and followers
  tweets.insert(tweet)
  users.update({_id: user[:_id]}, user, upsert: true)

  puts "Inserted: #{status.text}"
end
