require 'tweetstream'
require 'yaml'
require 'mongo'
require 'json'
require 'pry'

include Mongo

my_config = YAML.load_file('config.yaml')

db     = MongoClient.new.db(my_config['database'])
tweets = db.collection(my_config['tweets'])
users  = db.collection(my_config['users'])

tweets.drop
users.drop
